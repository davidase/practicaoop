class Empleado:
    def __init__(self, nombre, nomina):
        self.nombre = nombre
        self.nomina = nomina

    def Calcular_Impuestos(self):
        tasa_imp = 0.30
        impuestos = self.nomina * tasa_imp
        return impuestos

    def __str__(self):
        print("El empleado {name} debe pagar {tax:.2f} euros".format(name=self.nombre, tax=self.Calcular_Impuestos()))

empleado1 = Empleado("Pepe", 20000)
empleado2 = Empleado("Ana", 30000)

imp_total = empleado1.Calcular_Impuestos() + empleado2.Calcular_Impuestos()

empleado1.Imprime()
empleado2.Imprime()
print("Los impuestos a pagar en total son {:.2f} euros".format(imp_total))

